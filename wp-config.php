<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define( 'FS_METHOD', 'direct' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lms');

/** MySQL database username */
define('DB_USER', 'greenwhitedev');

/** MySQL database password */
define('DB_PASSWORD', 'tarzan');
/** login password: JDO!*uQY@1grheBpU!; */

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_-IXw{KGy4EvU%ow(X+)pPs}Y`g;5F*(}*k0SW6Rv4<.|I$h/x5IOWAu4cf?s!Rj');
define('SECURE_AUTH_KEY',  '%Kh/~[iMl#h*{ <oQ}d3Kosqh - :r?+;1Z[HO-}Y-AEft}vJ#t=U!j&#<7~}[vI');
define('LOGGED_IN_KEY',    'h)/8Tp/VKzlHS3i0T7m:K)W#u;Ur+#7.f**g;oToEd6tX>*qMA<se%Fj-c>}ji7#');
define('NONCE_KEY',        '^;VPgr[u.M9Q0C=3P5}IYxt9Dwpp9[CIN1SjFJ[ENW4^^pygkM?rAr$PzwDjFHMP');
define('AUTH_SALT',        'Bm>E,^?SRpFK[B`=XCVm|es8f(`TU;ns4o>;A; 3I8xFJ%rR<<Z]d%J?g?kGYPOw');
define('SECURE_AUTH_SALT', '`f9e5r^~v,nGu9aa9>v8_r.R>%Ld63toD-qn@f]2_-uam]TAsc&_fp4+=R~eOKk^');
define('LOGGED_IN_SALT',   'x$*`RS6,h|`;<UX^O}`79#y)ACO2l:8-aUGk~.2b,:XO17U@Af{lmIC%MjFT2^@X');
define('NONCE_SALT',       '5dya]}>zKfE#q/;*K8JeF>jK$j}d+TmBS8)I=jw9i9$`I| E  AlQ@`28E!PL8e#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mlms_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
