<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $uri Demo directory url
 */

$manifest = array();
$manifest['title'] = __('LMS', 'lms');
$manifest['screenshot'] = $uri . '/screenshot.png';
$manifest['preview_link'] = 'http://wedesignthemes.com/themes/lms/';