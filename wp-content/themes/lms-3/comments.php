<div class="commententries">
<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'lms'); ?></p>
<?php  return;
	endif;?>
    
    <h3 class="border-title"> <?php comments_number(__('No Comments', 'lms'), __('Comment ( 1 )', 'lms'), __('Comments ( % )', 'lms') );?><span> </span></h3>    
    <?php if ( have_comments() ) : ?>
    
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
                    <div class="navigation">
                        <div class="nav-previous"><?php previous_comments_link( __( 'Older Comments', 'lms'  ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments', 'lms') ); ?></div>
                    </div> <!-- .navigation -->
        <?php endif; // check for comment navigation ?>
        
        <ul class="commentlist">
     		<?php wp_list_comments( array( 'callback' => 'dttheme_custom_comments' ) ); ?>
        </ul>
    
    <?php else: ?>
		<?php if ( ! comments_open() ) : ?>
            <p class="nocomments"><?php _e( 'Comments are closed.', 'lms'); ?></p>
        <?php endif;?>    
    <?php endif; ?>
	
    <!-- Comment Form -->
    <?php if ('open' == $post->comment_status) : 
			$author = "<div class='column dt-sc-one-half first'><p><input id='author' name='author' type='text' placeholder='".__("Your Name", 'lms')."' required /></p></div>";
			$email = "<div class='column dt-sc-one-half'> <p> <input id='email' name='email' type='text' placeholder='".__("Your Email", 'lms')."' required /> </p></div>";
			$comment = "<p class='textarea-field'><textarea id='comment' name='comment' cols='5' rows='3' placeholder='".__("Your Comment", 'lms')."' ></textarea></p>";
				
            comment_form();?>
	<?php endif; ?>
</div>