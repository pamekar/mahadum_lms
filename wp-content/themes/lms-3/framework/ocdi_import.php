<?php

if ( ! function_exists( 'ocdi_import_files' ) ) :
function ocdi_import_files() {
  return array(
    array(
      'import_file_name'           => 'Demo Import 1',
      'categories'                 => array( 'Category 1', 'Category 2' ),
      'import_file_url'            => 'http://themes-demo.com/lms-ocdi/default/demo-content.xml',
      'import_widget_file_url'     => 'http://themes-demo.com/lms-ocdi/default/widgets.wie',
      'import_customizer_file_url' => 'http://themes-demo.com/lms-ocdi/default/customizer.dat',
      'import_preview_image_url'   => 'http://themes-demo.com/lms-ocdi/default/screenshot.png',
      'preview_url'                => 'http://lms.dttheme.com/',
    ),
  );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );
endif;

if ( ! function_exists( 'dgwork_after_import' ) ) :
function dgwork_after_import( $selected_import ) {
     global $wpdb;
     $table_prefix = $wpdb->base_prefix;
      $result = $wpdb->get_results("SELECT term_taxonomy_id FROM ".$table_prefix."term_taxonomy");
      foreach ($result as $row) { 
        $term_taxonomy_id = $row->term_taxonomy_id;
        $countresult = $wpdb->get_results("SELECT count(*) as count FROM ".$table_prefix."term_relationships WHERE term_taxonomy_id = '$term_taxonomy_id'");
        $count = $countresult[0]->count;
        $wpdb->query($wpdb->prepare("UPDATE ".$table_prefix."term_taxonomy SET count = %s WHERE term_taxonomy_id = %d",$count,$term_taxonomy_id));
      }

      $result = $wpdb->get_results("SELECT ID FROM ".$table_prefix."posts");
      foreach ($result as $row) {
        $post_id = $row->ID;
        $countresult = $wpdb->get_results("SELECT count(*) as count FROM ".$table_prefix."comments WHERE comment_post_ID = '$post_id' AND comment_approved = 1");
        $count = $countresult[0]->count;
        $wpdb->query($wpdb->prepare("UPDATE ".$table_prefix."posts SET comment_count = %s WHERE ID = %d",$count,$post_id)); 
    }
         //Set Menu

      $top_menu = get_term_by('name', 'Top Menu', 'nav_menu');
        $landing_menu = get_term_by('name', 'Landing Page', 'nav_menu');
        set_theme_mod( 'nav_menu_locations' , array( 
              'header_menu' => $top_menu->term_id, 
              'landingpage_menu' => $landing_menu->term_id 
             ) 
        );

       //Set Front page
       $page = get_page_by_title('Home');
       if ( isset( $page->ID ) ) {
        update_option( 'page_on_front', $page->ID );
        update_option( 'show_on_front', 'page' );
       }

       //Import Revolution Slider
       if ( class_exists( 'RevSlider' ) ) {
           $slider_array = array(
              get_template_directory()."/framework/revslider/about.zip",
              get_template_directory()."/framework/revslider/classic-carousel.zip",
              get_template_directory()."/framework/revslider/onepage.zip",
              get_template_directory()."/framework/revslider/sensei_home.zip",
              get_template_directory()."/framework/revslider/subscription.zip"
              );
 
           $slider = new RevSlider();
           foreach($slider_array as $filepath){
             $slider->importSliderFromPost(true,true,$filepath);  
           }      

      }

      //Import LayerSlider
      if( defined('LS_ROOT_PATH') ){
          $_POST['import_images'] = true;
          include_once LS_ROOT_PATH.'/classes/class.ls.importutil.php';
          $import = new LS_ImportUtil(get_template_directory()."/framework/layerslider/LayerSlider_Export.zip");
      }

      if(dttheme_is_plugin_active('multi-rating/multi-rating.php')){
		//function to remove the auto placement of the rating form in blog page
		$get_mr_general_settings = get_option_table_value('mr_general_settings');

	  }

}

add_action( 'pt-ocdi/after_import', 'dgwork_after_import' );
endif;

function ocdi_change_time_of_single_ajax_call() {
  return 10;
}
add_action( 'pt-ocdi/time_for_one_ajax_call', 'ocdi_change_time_of_single_ajax_call' );

?>